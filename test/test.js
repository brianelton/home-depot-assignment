var assert = require('assert');
utils = require('../public/js/utils.js');

describe('Caching', function() {
  describe('Set Cache', function() {
    it('localStorage should be able to be set with action, variable and data', function() {
      utils.dataCache("set", "http://www.google.ca", '{"test variable":"test data"}');
      var data = utils.dataCache("get", "http://www.google.ca");
      data = JSON.parse(data);
      assert.equal("test data", data["test variable"]);
    });
  });
});