# homedepot.ca Front-End Developer Exercise | Vanilla JavaScript Edition

## Installation

Clone the repo
```sh
$ git clone git@gitlab.com:brianelton/home-depot-assignment.git
```

Install the devDependencies.
```sh
$ cd homedepot-test
$ npm install
```

Install http-server globally
```sh
$ npm install http-server -g
```

Start server
```sh
$ npm run server
```
View site at http://127.0.0.1:8080

Watch SCSS files (if development is occurring)
```sh
$ npm run watch-scss
```

Run Mocha tests
```sh
$ npm run test
```

## Notes
### Performance
Without the availability of an external caching application, such as Akamai, this solution makes use of a sudo browser cache, using local storage. When requesting data, the getData function first checks to see if the data being requested already exists (using the url as the key.) If it exists it sends back the cached data, if not, it makes a request to the provided url, in this case, http://api.fixer.io
An improvement to this would be to invalidate the cached data after a given timeframe, to ensure the data is not stale. In the case of http://api.fixer.io, they apparently refresh their data daily, so I would invalidate the data at the same rate.

### Repeatable code
Without the use of a server, I have taken advantages of HTML 5 templates. I have also refrained from using IDs to target elements so that the same JS code can be used no matter how many times the component has been put on to the page.
I have also created a utils.js file and have tried to make them reusable no matter the context. The one asterix to that is that the getData function requires the an event object, which is then passed back. This has nothing to do with the getData function, but rather the rest of the code. Not ideal (the passing around of events), but it would be a change that I would make given more time.

### Validation
Not a lot of validation has been used in this example. Using inputs with type "Number" and select drop downs limits what can be entered, but as a backup, I am testing the amount field to be a valid number, and I'm making sure that the two select elements aren't the same (well, showing an error if the are and not fetching any data).

### Styling
I admittedly did not go overboard with the CSS, but did create what I did in SCSS, using three variable definitions for colours that are repeated, and one mixin for form field styling. node-sass is installed in this project and I have set up the ability to watch the scss files to aid development.

### Accessibility
The tools is mostly accessible. There are ways to improve it, but it is keyboard navigable, and form fields have labels associated with them to help with describing what's happening (although there is an issue with repeating IDs). There could be more aria-labels to describe what is happening, and when there are errors, they aren't properly associated with the element that causes the error.

### Unit tests
I have included Mocha with this project, but unfortunately the two functions that would best be suited to automated testing (dataCache and getData in /js/utils.js) use localStorage (for performance, as noted above), which is not available in the context of Node/Mocha. The theoretical test can be seen in test/test.js, but it will not pass.

### Todos
* Check mobile support - I didn't host this, so didn't get to check it on my phones
* bundle JS files
* install and configure Webpack - make use of webpack-dev-server (for autoreload) and bundling.

