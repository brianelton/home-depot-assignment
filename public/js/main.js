function supportsTemplate() {
    return 'content' in document.createElement('template');
  }
  
if (supportsTemplate()) {
    var node, t = document.querySelector('#converter-template');
    for(var i=0; i<3; i++){
        node = document.createElement("div");
        node.className = "column"
        document.body.appendChild(node);
        t.content.querySelector('.currency-converter').id = 'currency-converter-' + (i+1);
        document.getElementsByClassName("column")[i].appendChild(document.importNode(t.content, true));
    }
    var c = document.body.querySelectorAll('.currency-converter');
    for(var s=0;s<c.length;s++){
        var el = c[s];
        // set event listeners
        var a, d = el.querySelector('.amount-from');
        d.addEventListener("focus", inputFocus);
        d.addEventListener("blur", inputBlur);
        d.addEventListener("keyup", inputKeyup);
        d = el.getElementsByTagName('select');
        for(a=0;a<d.length;a++){
            d[a].addEventListener("change", validateFields);
        }
        d = el.querySelector('.disclaimer a');
        d.addEventListener("click", showDisclaimer);

        //set ids for accessibility
        el.querySelector('.amount-from').id = 'amount-from-' + (s+1);
        el.querySelector('.currency-from').id = 'currency-from-' + (s+1);
        el.querySelector('.amount-to').id = 'amount-to-' + (s+1);
        el.querySelector('.currency-to').id = 'currency-to-' + (s+1);
        d = el.getElementsByTagName('label');
        for(var a=0;a<d.length;a++){
            var f = d[a].htmlFor;
            d[a].htmlFor = f + "-" +(s+1);
        }
    }

    
  } else {
    console.log('no templates');
}
function inputFocus(event){
    if(event.target.value == "0.00")
        event.target.value = "";
}
function inputBlur(event){
    if(event.target.value == "")
        event.target.value = "0.00";
}
function inputKeyup(event){
    if((event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 8) {
        validateFields(event);
    } 
}
function validateFields(event){
    var valid = true;
    var parent = event.target.parentNode;
    parent.querySelector(".currency-error").style.display = "none";
    parent.querySelector(".amount-error").style.display = "none";
    

    // ensure sure amount is number
    var amount = parent.querySelector(".amount-from").value;
    if(Number.isNaN(amount)){
        parent.querySelector(".amount-error").style.display = "block";
        valid = false;
    }

    // ensure selected amounts aren't the same
    var currency_from = parent.querySelector(".currency-from").value;
    var currency_to = parent.querySelector(".currency-to").value;
    if(currency_from == currency_to) {
        parent.querySelector(".currency-error").style.display = "block";
        parent.querySelector(".amount-to").value = amount;
        valid = false;
    }

    if(valid)
        getCurrencyExchangeData(event, convertCurrency);
}
function showDisclaimer(event){
    event.preventDefault();
    //console.log("entering showDisclaimer");
    getCurrencyExchangeData(event, function(event, data){
        var currency = Object.keys(data.rates)[0];
        alert(data.base +" is being converted to " + currency + " at a rate of " + data.rates[currency] + " as of " +data.date);
    });
}
function getCurrencyExchangeData(event, callback){
    // getData
    //console.log("entering getCurrencyExchangeData");
    var parent = event.target.parentNode;
    if(parent.nodeName == "P") // there are a lot of assumptions made about the parentNode
        parent = parent.parentNode;
    var amount = parent.querySelector(".amount-from").value;
    var display_location = parent.querySelector(".amount-to");
    var base = parent.querySelector(".currency-from").value;
    var symbols = parent.querySelector(".currency-to").value;

    getData("http://api.fixer.io/latest?base="+base+"&symbols="+symbols, event, function(event, data){
        //console.log("getData Callback");
        data = JSON.parse(data);
        // if the ajax call is not working
        if(data.error){
            alert(data.error);
        } else {
            callback(event, data);
        }
    });
}

function convertCurrency(event, data){
    //console.log("entering convertCurrency");
    var parent = event.target.parentNode;
    var amount = parent.querySelector(".amount-from").value;
    var display_location = parent.querySelector(".amount-to");
    var symbols = parent.querySelector(".currency-to").value;
    display_location.value = (amount * data.rates[symbols]).toFixed(2); 
} 

