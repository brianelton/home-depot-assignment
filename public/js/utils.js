function dataCache (action, key, data) {
    switch (action) {
        case "set":
             localStorage.setItem(key, data);
             return true;    
         case "get":
             return localStorage.getItem(key);
         case "remove":
             localStorage.removeItem(key);
             return true;
        default:
         return null
    }
     
 }
 function getData (url, event, callback) {
     var data = dataCache("get", url);
     if (data) {
         console.log('Loading data from cached');
         callback(event, data);
     } else {
         console.log("Loading data from ajax");
         var xhttp = new XMLHttpRequest();
         xhttp.onreadystatechange = function() {
           if (this.readyState == 4 && this.status == 200) {
             dataCache("set",url,this.responseText);
             callback(event, this.responseText);
           } else if (this.readyState == 4 && this.status != 200){
               data = JSON.stringify({"error":"An error has occured. Please try again later"});
               callback(event, data);
           }
         };
         xhttp.open("GET", url, true);
         xhttp.send();
     }
 }

if (typeof module !== 'undefined' && module.exports != null) {
    exports.dataCache = dataCache;
    exports.getData = getData;
}